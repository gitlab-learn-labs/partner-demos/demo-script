# Demo Script

## Introduction
 
This group contains all of the projects you would need to do a quick demo in an hour or less. There are two main projects within this group, ***partner-demo*** & ***partner-demo-merged***, but you most likely will only need to fork the ***partner-demo*** one. The merged workshop is there so you can see how you may set up your group if you are trying to demo most of the security ultimate features that come with GitLab. The goal of the quick demo is to show how you could use the capabilities of Gitlab as a one stop shop.
 
## Getting started
 
You will need to be able to [connect your project to some flavor of kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/). For our example we used the GitLab agent for Kubernetes along with GCP, but feel free to do whatever you are most comfortable with. Once you have forked the project over into your own namespace and have it connected you should be ready to go.

Resetting each project will depend on what you do if you have merged the new branch into main or not. If you haven't, you most likely will just need to close a few issues and an MR then you will be ready to go. Otherwise if you have merged you can re-fork the project or revert the merge to reset the code base.
 
## Talk Track
 
### Part 1: Organize, Track, and Define Work

Within the project view click the issue tab, create a new issue to address how we will add new security capabilities to our pipeline. Go through the various options, highlighting how you can create and add new labels for this issue. Once the issue is created go ahead and go to the board view and show off how easy it is to edit on the fly, create new lists based off labels, ect. Once done go to the Merge Request tab and create a new mr to merge our completed-ppipeline into main. Our cicd isnt done yet though, so you can then take a quick jump to the cicd editor, swap to our new branch, and copy in the new pipeline below. The completed pipeline will stay in the completed branch if a presenter needed to re-pull up a finished pipeline. Next spend some time to show off the cicd capabilities. Some good things to call out are how to drill down into templates and how we are not plugin driven as plugins always bite you in the end when they stop getting updated.

```
image: docker:latest

services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  ROLLOUT_RESOURCE_TYPE: deployment

stages:
  - build
  - unit
  - test
  - feature
  - staging
  - cleanup
  - production

include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml

build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE

unit:
  image: python:latest
  stage: unit
  script:
    - apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
    - curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash
    - apt install gcc libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev openssl -y
    - pip install --upgrade pip
    - pip3 install -r requirements.txt
    - python -m unittest tests/test_db.py

gemnasium-python-dependency_scanning:
  variables:
    SECURE_LOG_LEVEL: "debug"
    RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
    CI_DEBUG_TRACE: “true”
    DS_REMEDIATE: "true"
    GIT_STRATEGY: fetch
  before_script:
    - apt update -y
    - apt install curl -y
    # - apt install gcc libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev openssl -y
    - apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
    - curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash
    - apt install gcc libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev openssl -y
```

### Part 2: Shifting Left

The second part of the demo should focus on the ***partner-demo*** project primarily around the MR and the pipeline that ran. Highlight all of the MR capabilities, primarily the security report and how you can act on some of the results. Make a comment,  a new issue, ect and explain the process of shifting left and how all of this was done at the first commit (including the review app) and how much time that can save you instead of waiting until the security or QA team catches your mistake months later. If you are just looking for something quick you can wrap up here as you have touched on SCM, PM, CICD, and Security with the possibility of doing a deeper dive in the future.
 
### Part 3: Security Reports (Ultimate heavy)
 
***Note: This part of the workshop heavily focuses on Ultimate features. If the customer has already indicated that they are not interested in Ultimate you do not need to demo this section.***

Next depending on your time you can either merge the existing merge request or just swap over to the ***partner-demo-merged*** to showcase how the pipeline ran, touch on many of the scanners, and also highlight the capabilities of gitlab ci/cd. Make sure you click through at least one job and all of the tabs in the pipeline view. Next you can go down all of the security reports under the **Security & Compliance** tab on the left hand navigation menu.
 
**Security Dashboard**
 
Note that this typically takes overnight to populate. If you are not using a pre-merged project, I would have a different example ready to go
 
**Vulnerability Report**
 
Don't have to spend much time here as this is what is already seen on the mr and pipeline. Just mention that this report will always show what has been reported for the main branch.
 
**On Demand Scan**
 
can show it or just mention it, setting it up is extra credit if you have time or they ask for it. More details below
 
**Dependency List**
 
This is also the SBOM Report. Show how to download cyclone DX results and mention solar winds leading to Biden Admin requiring this if US based & working with gov agencies. https://docs.gitlab.com/ee/user/application_security/dependency_list/
 
**License Compliance**
 
Similar to dependency, highlight how you could set one of them to be disallowed.
 
**Policies**
 
Talk about what you can set, but actually setting them is extra credit. 
 
**Audit Events**
 
Just show what events are auto tracked and how its useful in war rooms.
 
**Config**
 
Not super important as most of this will be configured in the pipeline.
 
## Extra Credit
 
1. Set up a policy to disallow merging if there is a secret detected. Use the left hand navigation menu to click through **Security & Compliance > Policies**, click **New policy**, and set up a ***Scan result policy***. Once its merged you could re-launch an MR to show how the secrect we already have in the code is now blocking the merge.
 
2. Set up an on demand scan depending on the wants of your customer. This one is pretty straight forward, but can be the cherry on top of a good demo if they are really into one scanner like SAST for example. 
 
3. Have a compliance pipeline defined at the group level and apply it. Explain how this will force pipeline compliance for all teams: https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#configure-a-compliance-pipeline

